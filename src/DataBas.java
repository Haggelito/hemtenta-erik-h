import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DataBas {
	public static String databas = "flygplan";

	private static String url = "jdbc:mysql://localhost:3306/";
	private static String user = "root";
	private static String pass = "innebandy123";
	private static Connection conn;
	private static Statement myStmt;
	// private static ResultSet myRs;
	String test;

	public DataBas() {
		

		try {
			conn = DriverManager.getConnection(url, user, pass);
			Class.forName("com.mysql.jdbc.Driver");
			myStmt = conn.createStatement();

		} catch (ClassNotFoundException | SQLException exc) {
			exc.printStackTrace();
		}
	}

	public void skapaDb() {
		//execute SQL query
		try {
			myStmt.executeUpdate("CREATE DATABASE IF NOT EXISTS " + databas
					+ ";");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".flygplats(flygplats_id INT NOT NULL AUTO_INCREMENT, flygplats_namn VARCHAR(45) UNIQUE NOT NULL, PRIMARY KEY (flygplats_id));");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".flygbolag(	flygbolag_id INT NOT NULL AUTO_INCREMENT,flygbolag_namn VARCHAR(45) UNIQUE NOT NULL,PRIMARY KEY (flygbolag_id));");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".flyglinje(flyglinje_id INT NOT NULL AUTO_INCREMENT, avgang VARCHAR(45) NOT NULL, destination VARCHAR(45) NOT NULL, flygplan_id INT NOT NULL, datum VARCHAR(45) NOT NULL,PRIMARY KEY (flyglinje_id));");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".flygplan(flygplan_id INT NOT NULL AUTO_INCREMENT, flygbolag_id INT NOT NULL,PRIMARY KEY (flygplan_id));");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".kund(kund_id INT NOT NULL AUTO_INCREMENT,fornamn VARCHAR(45) NOT NULL,efternamn VARCHAR(45) NOT NULL,telefonnummer VARCHAR(45) NOT NULL, PRIMARY KEY (kund_id));");
			myStmt.executeUpdate("CREATE TABLE IF NOT EXISTS "
					+ databas
					+ ".bokning(bokning_id INT NOT NULL AUTO_INCREMENT,kund_id VARCHAR(45) NOT NULL,flyglinje VARCHAR(45) NOT NULL,PRIMARY KEY (bokning_id));");
			myStmt.executeUpdate("INSERT IGNORE INTO "
					+ databas
					+ ".flyglinje(avgang, destination, flygplan_id, datum) VALUES ('OSD','ARN',1,'2014-10-22');");
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	
	public void inmatningDb(String query) {
		try {
			myStmt.executeUpdate(query);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	public void listaFlyglinjer() {

		try {
			ResultSet myRs = myStmt.executeQuery("SELECT * FROM " + databas
					+ ".flyglinje");
			System.out.println("----Flyglinjer----");
			while (myRs.next()) {

				int idflyg = myRs.getInt("flyglinje_id");
				String avgang = myRs.getString("avgang");
				String destination = myRs.getString("destination");
				String datum = myRs.getString("datum");
				// String aNamn = myRs.getString("flygplats");
				System.out.println(" " + idflyg + ".\t" + avgang + "\t"
						+ destination + "\t" + datum + "\t");
			}
			System.out.println("------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	public void listaFlygplatser() {

		try {
			ResultSet myRs = myStmt.executeQuery("SELECT * FROM " + databas
					+ ".flygplats ORDER BY flygplats_id ASC");
			System.out.println("Tillg�ngliga flygplatser.");
			while (myRs.next()) {

				int idflyg = myRs.getInt("flygplats_id");
				String aNamn = myRs.getString("flygplats_namn");

				System.out.println(" " + idflyg + ".\t" + aNamn + "\t");
			}
			System.out.println("------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}

	}

	
	public String hamtaData(int id,String query){
		System.out.println("hamtaData() " );
		String namn = null;
		
		try {
			ResultSet myRs = myStmt.executeQuery(query);
			System.out.println();
			while (myRs.next()) {

				
				namn = myRs.getString(id);
				
				
			}
			
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}
		
		return namn;
	}
	
	public void listaFlygbolag(){
		
		try {
			ResultSet myRs = myStmt.executeQuery("SELECT * FROM " + databas
					+ ".flygbolag");
			System.out.println("Tillg�ngliga flygbolag.");
			while (myRs.next()) {

				int idflygbolag = myRs.getInt("flygbolag_id");
				String aNamn = myRs.getString("flygbolag_namn");

				System.out.println(" " + idflygbolag + ".\t" + aNamn + "\t");
			}
			System.out.println("------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}
		
	}
	
	public void listaBokningar() {
		
		try {
			ResultSet myRs = myStmt.executeQuery("SELECT * FROM " + databas
					+ ".bokning");
			System.out.println("----Bokningar.----");
			while (myRs.next()) {

				int idbokning = myRs.getInt("bokning_id");
				int idkund = myRs.getInt("kund_id");
				String flyglinje = myRs.getString("flyglinje");

				System.out.println(" " + idbokning + ".\t" + idkund + "\t" + flyglinje + "\t" );
			}
			System.out.println("------------------");
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}
		
		
	}

	
	
	
	public static void close() {

		try {

			conn.close();
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

}

