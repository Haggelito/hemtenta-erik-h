
import java.util.Scanner;
import java.sql.ResultSet;

public class Administrator {
	
		DataBas koppling = new DataBas();
		String flygbolag;
		String flyglinje;
		String destination;
		String inputString;
		String flygplats;
		String bokningar;
		RegEx valedering = new RegEx();
		Scanner input = new Scanner(System.in);
		
		
			//metod
			public void menyAdmin(){
			int menyval = 0;
			System.out.println("1. skapa flygbolag");
			System.out.println("2. skapa flygplats");
			System.out.println("3. skapa flyglinje");
			System.out.println("4. lista flyglinjer");
			System.out.println("5. lista bokningar");
			do{
			switch(menyval){
			
			case 1:
			System.out.println("skapa flygbolag");
			skapaFlygbolag();
			break;
			
			case 2:
			System.out.println("skapa flygplats");
			skapaFlygplats();
			break;
			
			case 3:
			System.out.println("skapa flyglinje");
			skapaFlyglinjer();
			break;
			
			case 4:
			System.out.println("lista flyglinjer");
			listaFlyglinjer();
			break;
			
			case 5:
			System.out.println("lista bokningar");
			listaBokningar();
			break;
			
			
			}
			}while(!(menyval >= 5));
			
		}
		
		public void skapaFlygplats(){
			String val = null;
			do {
			System.out.println("Vilken flygplats?");
			flygplats = input.next();
			if(!valedering.valederingFlygPlats(flygplats)){
				System.out.println("försök igen");
			}
			}while(!valedering.valederingFlygPlats(flygplats));
			FlygPlats flygplats = new FlygPlats(val);
			koppling.inmatningDb("INSERT IGNORE INTO " + DataBas.databas
					+ ".flygplats(flygplats_namn) VALUES ('" + flygplats.getNamn()
					+ "')");
			
			
		}
		public void skapaFlygbolag(){
			String val = null;
			do{
				System.out.println("Vilket flygbolag?");
				flygbolag = input.next();
				if(!valedering.valederingFlygBolag(flygbolag)){
					System.out.println("försök igen");
				}
				}while(!valedering.valederingFlygBolag(flygbolag));
				FlygBolag flygbolag = new FlygBolag(val);
				koppling.inmatningDb("INSERT IGNORE INTO " + DataBas.databas
					+ ".flygbolag(flygbolag_namn) VALUES ('" + flygbolag.getNamn()
					+ "')");
				
		}
		
		public void skapaFlyglinjer(){
			String query;
			int kolid;
			String destination;
			int flygplan_id = 0;
			String datum;

			// do {
			// skapa ett object
			FlygLinje flyglinje = new FlygLinje();

			koppling.listaFlygplatser();
			
			System.out.println("V�lj avg�ngsflygplats via id.");
			kolid = input.nextInt();

			query = "SELECT flygplats_namn from bokningssystem.flygplats where flygplats_id ="
					+ kolid + ";";

			String avgangsFlygplats = koppling.hamtaData(kolid, query);
			flyglinje.setAvgang(koppling.hamtaData(kolid, query));
			System.out.println(avgangsFlygplats);

			System.out.println("V�lj ankomstflyplats via id.");
			kolid = input.nextInt();

			query = "SELECT flygplats_namn from bokningssystem.flygplats where flygplats_id ="
					+ kolid + "";

			String ankomstFlygplats = koppling.hamtaData(kolid, query);
			flyglinje.setAnkomst(koppling.hamtaData(kolid, query));
			
			System.out.println(ankomstFlygplats);
			
			koppling.listaFlygbolag();
			
			System.out.println("V�lj flygbolag via id.");
			kolid = input.nextInt();

			query = "SELECT flygbolag_namn from bokningssystem.flygbolag where flygbolag_id ="
					+ kolid + "";

			String flygbolag = koppling.hamtaData(kolid, query);
			flyglinje.setFlygbolag(koppling.hamtaData(kolid, query));
			
			System.out.println(flygbolag);
			
			System.out.println("Ange datum.");
			datum = input.next();
			
			
			/*System.out.println("Ange flyglinjens namn.");
			flygplan_id = input.nextInt();*/

			 koppling.inmatningDb("INSERT INTO "+DataBas.databas +".flyglinje(avgang, destination, flygplan_id, datum) VALUES ('" +flyglinje.getAvgang()+ "' '" +flyglinje.getAnkomst()+ "' '" +flyglinje.getFlygbolag()+ "' '" +datum+ "')");
			System.out.println("Flyglinje �r nu skapad.");

			menyAdmin();
		

			
		}
		
		public void listaFlyglinjer(){
			koppling.listaFlyglinjer();
		}
		
		public void listaBokningar(){
		
			koppling.listaBokningar();

	}
		
}



